package com.mitocode.controller;


import com.mitocode.dto.PatientDTO;
import com.mitocode.model.Patient;
import com.mitocode.service.PatientServiceImpl;
import jakarta.validation.Valid;
import java.net.URI;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/patients")
@RequiredArgsConstructor
public class PatientController {
  private final PatientServiceImpl service;

  @Qualifier("defaultMapper")
  private final ModelMapper mapper;

  @GetMapping
  public ResponseEntity<List<PatientDTO>> findAll() {
    List<PatientDTO> lst = service.findAll().stream().map(this::convertToDto).toList();
    return new ResponseEntity<>(lst, HttpStatus.OK);
  }

  @GetMapping("{id}")
  public ResponseEntity<PatientDTO> findById(@PathVariable("id") Integer id) {
    Patient obj = service.findById(id);
    return new ResponseEntity<>(this.convertToDto(obj), HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity<PatientDTO> save(@Valid @RequestBody  PatientDTO dto) {
    Patient obj = service.save(this.convertToEntity(dto));
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdPatient()).toUri();
    return ResponseEntity.created(location).build();
  }

  @PutMapping("/{id}")
  public ResponseEntity<PatientDTO> update(@Valid @PathVariable("id") Integer id, @RequestBody  PatientDTO dto)
      throws Exception {
    // dto.setIdPatient(id);
    Patient obj = service.update(convertToEntity(dto), id);
    return new ResponseEntity<>(convertToDto(obj), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> delete(@PathVariable("id") Integer id) {
    service.delete(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  private PatientDTO convertToDto(Patient obj) {
    return mapper.map(obj, PatientDTO.class);
  }

  private Patient convertToEntity(PatientDTO dto) {
    return mapper.map(dto, Patient.class);
  }
}
