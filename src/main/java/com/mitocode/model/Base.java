package com.mitocode.model;

import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.Column;
import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
public class Base {
  public String createdBy;
  public String updatedBy;

  @CreationTimestamp
  @Column(updatable = false)
  public LocalDateTime createdAt;

  @UpdateTimestamp
  public LocalDateTime updatedAt;
}
